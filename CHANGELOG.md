## [1.1.0] - 2020-01-24
- Clip frame start index is added.
- Clip frame unit speed is added.
- Clip continue clip is added.
- Clip priority is added.
- Clip ignoreSelf is added.


## [1.0.0] - 2020-01-01
- Unity Package Manager(UPM) is supported.
