using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BSS.SpriteAnimation
{
    /// <inheritdoc />
    public class FrameAnimator : IFrameAnimator
    {
        /// <inheritdoc />
        public Action OnClipComplete { get; set; }

        /// <inheritdoc />
        public Action<int> OnFrameChanged { get; set; }

        /// <inheritdoc />
        public Action<string> OnTrigger { get; set; }

        /// <inheritdoc />
        public IClip CurrentClip { get { return _currentClip; } }

        /// <inheritdoc />
        public bool IsPlaying { get { return _isPlaying; } }

        /// <inheritdoc />
        public AnimatorChildMode ChildMode { get { return _childMode; } }

        internal IClip[] _clips = null;
        IClip _currentClip = null;
        IAnimator[] _children;
        Dictionary<string, IClip> _nameToClip = new Dictionary<string, IClip>();
        bool _isPlaying;
        AnimatorChildMode _childMode;
        float _currentTime;
        int _currentFrameIndex;

        /// <summary>
        /// CTOR
        /// </summary>
        /// <param name="clips">(Optional) The clips associated with this animator</param>
        /// <param name="childMode">(Optional) How to handle the relationship with a parent animator</param>
        public FrameAnimator(IClip[] clips = null, AnimatorChildMode childMode = AnimatorChildMode.PlayWithParent)
        {
            _clips = clips;
            _childMode = childMode;

            if (clips != null)
            {
                for (var i = 0; i < clips.Length; i++)
                {
                    var clip = clips[i];
                    _nameToClip.Add(clip.Name, clip);
                }
            }
        }

        /// <inheritdoc />
        public void SetChildren(IAnimator[] children)
        {
            if (children != null && children.Contains(this))
            {
                throw new System.InvalidOperationException("The list of children cannot contain the parent animator. This would cause an infinite loop");
            }

            _children = children;
        }

        /// <inheritdoc />
        public bool Flip
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        /// <inheritdoc />
        public void Play(IClip clip)
        {
            if (_currentClip!=null) {
                if(_currentClip == clip && _currentClip.IgnoreSelf) return;
                if(_currentClip.Priority > clip.Priority) return;
            }
            _currentClip = clip;
            _currentTime = 0f;
            _currentFrameIndex = 0;
            HandleFrame(_currentFrameIndex);
            _isPlaying = true;

            // Play Children
            if(_children != null && _children.Length > 0) {
                for(var i = 0; i < _children.Length; i++) {
                    var animator = _children[i];
                    if(animator != null && animator.ChildMode != AnimatorChildMode.IgnoreParent) {
                        if(animator.ChildMode == AnimatorChildMode.PlayWithParent) {
                            animator.Play(clip.Name);
                        } else if(animator.ChildMode == AnimatorChildMode.ShareClipsWithParent) {
                            animator.Play(clip);
                        }
                    }
                }
            }
        }
        private void ForcePlay(string name) {
            var clip = _nameToClip[name];
            _currentClip = clip;
            _currentTime = 0f;
            _currentFrameIndex = 0;
            HandleFrame(_currentFrameIndex);
            _isPlaying = true;

            // Play Children
            if(_children != null && _children.Length > 0) {
                for(var i = 0; i < _children.Length; i++) {
                    var animator = _children[i];
                    if(animator != null && animator.ChildMode != AnimatorChildMode.IgnoreParent) {
                        if(animator.ChildMode == AnimatorChildMode.PlayWithParent) {
                            animator.Play(clip.Name);
                        } else if(animator.ChildMode == AnimatorChildMode.ShareClipsWithParent) {
                            animator.Play(clip);
                        }
                    }
                }
            }
        }

        /// <inheritdoc />
        public void Play(string name)
        {
            Play(_nameToClip[name]);
        }

        /// <inheritdoc />
        public void Pause()
        {
            _isPlaying = false;

            // Pause Children
            if (_children != null && _children.Length > 0)
            {
                for (var i = 0; i < _children.Length; i++)
                {
                    var animator = _children[i];
                    if (animator != null && animator.ChildMode != AnimatorChildMode.IgnoreParent)
                    {
                        animator.Pause();
                    }
                }
            }
        }

        /// <inheritdoc />
        public void Resume()
        {
            _isPlaying = true;

            // Resume Children
            if (_children != null && _children.Length > 0)
            {
                for (var i = 0; i < _children.Length; i++)
                {
                    var animator = _children[i];
                    if (animator != null && animator.ChildMode != AnimatorChildMode.IgnoreParent)
                    {
                        animator.Resume();
                    }
                }
            }
        }

        /// <inheritdoc />
        public void Tick(float deltaTime)
        {
            if (_isPlaying && _currentClip != null)
            {
                var currentFrame = _currentClip[_currentFrameIndex];
                _currentTime += deltaTime * _currentClip.FramesPerSeconds * currentFrame.Speed;
                if (_currentTime >= 1f)
                {
                    _currentTime -= 1f;
                    _currentFrameIndex++;
                    if (_currentFrameIndex >= _currentClip.FrameCount)
                    {
                        if (!_currentClip.Loop)
                        {
                            OnClipComplete?.Invoke();
                            if (!string.IsNullOrEmpty(_currentClip.ContinueClipName)) {
                                ForcePlay(_currentClip.ContinueClipName);
                            } else {
                                Pause();
                            }
                            return;
                        }
                        _currentFrameIndex = 0; // Loop
                    }
                    HandleFrame(_currentFrameIndex);
                }
            }
        }

        /*
        Invokes the appropriate actions of a given frame
        */
        void HandleFrame(int frame)
        {
            var currentFrame = _currentClip[frame];
            if (OnFrameChanged != null) { OnFrameChanged(currentFrame.Index); }
            if (currentFrame.HasTrigger)
            {
                if (OnTrigger != null) { OnTrigger(currentFrame.TriggerName); }
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            OnClipComplete = null;
            OnFrameChanged = null;
            OnTrigger = null;
            _currentClip = null;
        }
    }

    /// <inheritdoc />
    [Serializable]
    public class Clip : IClip
    {
        [SerializeField, Tooltip("The name of the clip")]
        private string _name;
        [SerializeField, Tooltip("Wether this clip loops or not")]
        private bool _loop = true;
        [SerializeField, Tooltip("The frame rate of this clip in frames per second")]
        private float _framesPerSeconds = 8f;
        [SerializeField, Tooltip("If the clip being played has a higher priority, it is ignored.")]
        private int _priority = 0;
        [SerializeField, Tooltip("If true, playing is ignored again.")]
        private bool _ignoreSelf = true;
        [SerializeField, Tooltip("Play clip name when this clip ended [Must be not loop]")]
        private string _continueClipName = "";

        [Header("Frame Option")]
        [SerializeField, Tooltip("The frame unit speed (Negative Value: Ignore)")]
        internal int _frameUnitSpeed = 1;
        [SerializeField, Tooltip("The frame start index (Negative Number: Ignore)")]
        internal int _frameStartIndex = 0;
        [SerializeField, Tooltip("The frames of this clip")]
        private List<Frame> _frames=new List<Frame>();

        public string Name { get { return _name; } }
        public bool Loop { get { return _loop; } }
        public float FramesPerSeconds { get { return _framesPerSeconds; } }
        public int Priority { get { return _priority; } }
        public bool IgnoreSelf { get { return _ignoreSelf; } }

        public string ContinueClipName { get { return _continueClipName; } }
        public int FrameCount { get { return _frames.Count; } }
        public IFrame this [int index] { get { return _frames[index]; } }
    }

    /// <inheritdoc />
    [Serializable]
    public class Frame : IFrame
    {
        [SerializeField, Tooltip("The index of this clip")]
        int _index = 0;
        [SerializeField, Tooltip("The speed of this clip. Use this to slow down or speed up individual frames")]
        float _speed = 1f;
        [SerializeField, Tooltip("The name of the trigger. Leave empty if you don't want a trigger")]
        string _triggerName;

        /// <inheritdoc />
        public int Index { get { return _index; } set{ _index = value; } }

        /// <inheritdoc />
        public float Speed { get { return _speed; } set { _speed = value; } }

        /// <inheritdoc />
        public bool HasTrigger { get { return !string.IsNullOrEmpty(_triggerName); } }

        /// <inheritdoc />
        public string TriggerName { get { return _triggerName; } }
    }
}