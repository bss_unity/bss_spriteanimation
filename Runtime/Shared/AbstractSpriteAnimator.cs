using Sirenix.OdinInspector;
using System;
using System.Linq;
using UnityEngine;

namespace BSS.SpriteAnimation
{
    public abstract class AbstractSpriteAnimator : MonoBehaviour, IAnimator
    {
        [SerializeField, Tooltip("A list of sprites to use with this animator. A frames index directly corresponds to an index in this list.")]
        protected UnityEngine.Sprite[] _sprites;
        [SerializeField, Tooltip("Play clip with name when Start.")]
        protected string startClipName="";
        [SerializeField, Tooltip("The clips that can be used by this animator")] 
        public Clip[] _clips;
        [SerializeField, Tooltip("If true, any children under this animator will be played, paused and resumed together with this parent")] protected bool _animateChildren = true;
        [SerializeField, Tooltip("How to handle the relationship with a parent animator")]
        AnimatorChildMode _childMode;
        [SerializeField, Tooltip("If false, any attempts to flip this image is suppressed")]
        protected bool _allowFlipping = true;

        internal IFrameAnimator _frameAnimator;

        /// <inheritdoc />
        public Action OnClipComplete { get; set; }

        /// <inheritdoc />
        public Action<string> OnTrigger { get; set; }

        [ShowInInspector]
        /// <inheritdoc />
        public bool IsPlaying { get { return _frameAnimator.IsPlaying; } }

        [ShowInInspector]
        /// <inheritdoc />
        public IClip CurrentClip { get { return _frameAnimator.CurrentClip; } }

        /// <inheritdoc />
        public AnimatorChildMode ChildMode { get { return _childMode; } }

        /// <inheritdoc />
        public abstract bool Flip { get; set; }

        /// <summary>
        /// Invoked when this animator is created
        /// </summary>
        protected abstract void Init();

        /// <summary>
        /// Invoked when the frame of the animation changes
        /// </summary>
        protected abstract void HandleFrameChanged(int index);

        void HandleClipComplete()
        {
            if (OnClipComplete != null) { OnClipComplete(); }
        }

        void HandleTrigger(string triggerName)
        {
            if (OnTrigger != null) { OnTrigger(triggerName); }
        }

        /// <inheritdoc />
        public void Play(IClip clip)
        {
            _frameAnimator.Play(clip);
        }

        /// <inheritdoc />
        public void Play(string clipName)
        {
            _frameAnimator.Play(clipName);
        }

        /// <inheritdoc />
        public void Pause()
        {
            _frameAnimator.Pause();
        }

        /// <inheritdoc />
        public void Resume()
        {
            _frameAnimator.Resume();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            OnClipComplete = null;
            OnTrigger = null;
            _frameAnimator.Dispose();
            _frameAnimator = null;
        }

        #region MonoBehavior (Unity)

        void Awake()
        {
            _frameAnimator = new FrameAnimator(_clips, _childMode);
            Init();
        }

        void Start()
        {
            if (_animateChildren)
            {
                var childAnimators = GetComponentsInChildren<IAnimator>().Where(x => !x.Equals(this)).ToArray();
                _frameAnimator.SetChildren(childAnimators);
            }

            // Start first animation
            if (!string.IsNullOrEmpty(startClipName))
            {
                _frameAnimator.Play(startClipName);
            }
        }

        void OnEnable()
        {
            _frameAnimator.OnClipComplete += HandleClipComplete;
            _frameAnimator.OnFrameChanged += HandleFrameChanged;
            _frameAnimator.OnTrigger += HandleTrigger;
        }

        void OnDisable()
        {
            _frameAnimator.OnClipComplete -= HandleClipComplete;
            _frameAnimator.OnFrameChanged -= HandleFrameChanged;
            _frameAnimator.OnTrigger -= HandleTrigger;
        }

        void Update()
        {
            _frameAnimator.Tick(Time.deltaTime);
        }

        void OnDestroyed()
        {
            Dispose();
        }

        void OnValidate() {
            if(_clips == null)
                return;
            foreach (var clip in _clips) {
                if (clip._frameStartIndex>=0) {
                    for(int i = 0; i < clip.FrameCount; i++) {
                        clip[i].Index = clip._frameStartIndex + i;
                    }
                }
                if(clip._frameUnitSpeed > 0) {
                    for(int i = 0; i < clip.FrameCount; i++) {
                        clip[i].Speed = clip._frameUnitSpeed;
                    }
                }
            }
        }

        #endregion
    }
}