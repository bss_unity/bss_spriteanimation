namespace BSS.SpriteAnimation
{
    public enum AnimatorChildMode
    {
        PlayWithParent,
        ShareClipsWithParent,
        IgnoreParent,
    }
}